EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "inenvmon_breakout"
Date "2020-05-21"
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x14 J6
U 1 1 5EF26317
P 7250 3600
F 0 "J6" H 7330 3592 50  0000 L CNN
F 1 "Conn_01x14" H 7330 3501 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 7250 3600 50  0001 C CNN
F 3 "~" H 7250 3600 50  0001 C CNN
	1    7250 3600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J7
U 1 1 5EF2740D
P 7800 3600
F 0 "J7" H 7718 4417 50  0000 C CNN
F 1 "Conn_01x14" H 7718 4326 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 7800 3600 50  0001 C CNN
F 3 "~" H 7800 3600 50  0001 C CNN
	1    7800 3600
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x16_Counter_Clockwise J2
U 1 1 5EF2E3ED
P 1500 3800
F 0 "J2" H 1550 2775 50  0000 C CNN
F 1 "Conn_02x16" H 1550 2866 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x16_P2.54mm_Vertical" H 1500 3800 50  0001 C CNN
F 3 "~" H 1500 3800 50  0001 C CNN
	1    1500 3800
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x18 J5
U 1 1 5EF41D93
P 5500 3650
F 0 "J5" H 5418 4667 50  0000 C CNN
F 1 "Conn_01x18" H 5418 4576 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x18_P2.54mm_Vertical" H 5500 3650 50  0001 C CNN
F 3 "~" H 5500 3650 50  0001 C CNN
	1    5500 3650
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5F0202BF
P 8650 5000
F 0 "R1" H 8720 5046 50  0000 L CNN
F 1 "10k" H 8720 4955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8580 5000 50  0001 C CNN
F 3 "~" H 8650 5000 50  0001 C CNN
	1    8650 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F023F44
P 8950 5000
F 0 "R2" H 8880 4954 50  0000 R CNN
F 1 "10k" H 8880 5045 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8880 5000 50  0001 C CNN
F 3 "~" H 8950 5000 50  0001 C CNN
	1    8950 5000
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J8
U 1 1 5EF288B9
P 9450 4500
F 0 "J8" H 9500 4817 50  0000 C CNN
F 1 "Conn_02x04" H 9500 4726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x04_P2.54mm_Vertical" H 9450 4500 50  0001 C CNN
F 3 "~" H 9450 4500 50  0001 C CNN
	1    9450 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x18 J4
U 1 1 5EF4099B
P 4850 3650
F 0 "J4" H 4930 3642 50  0000 L CNN
F 1 "Conn_01x18" H 4930 3551 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x18_P2.54mm_Vertical" H 4850 3650 50  0001 C CNN
F 3 "~" H 4850 3650 50  0001 C CNN
	1    4850 3650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J9
U 1 1 5EF291AD
P 9100 3250
F 0 "J9" H 9180 3242 50  0000 L CNN
F 1 "Conn_01x08" H 9180 3151 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 9100 3250 50  0001 C CNN
F 3 "~" H 9100 3250 50  0001 C CNN
	1    9100 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3650 4400 4250
Wire Wire Line
	4400 4250 4650 4250
Wire Wire Line
	5700 3150 6100 3150
Wire Wire Line
	6100 3150 6100 3750
Wire Wire Line
	6100 3750 6100 4350
Wire Wire Line
	6100 4350 5700 4350
Connection ~ 6100 3750
$Comp
L Device:R R3
U 1 1 5F024F81
P 10050 5000
F 0 "R3" H 9980 4954 50  0000 R CNN
F 1 "10k" H 9980 5045 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9980 5000 50  0001 C CNN
F 3 "~" H 10050 5000 50  0001 C CNN
	1    10050 5000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6000 3050 6000 3650
Wire Wire Line
	4650 3150 4300 3150
Wire Wire Line
	4300 3150 4300 3750
Wire Wire Line
	4300 3750 4650 3750
Wire Wire Line
	4300 3750 4300 4350
Wire Wire Line
	4300 4350 4650 4350
Connection ~ 4300 3750
$Comp
L power:GND #PWR0101
U 1 1 5F127CE0
P 4400 4750
F 0 "#PWR0101" H 4400 4500 50  0001 C CNN
F 1 "GND" H 4405 4577 50  0000 C CNN
F 2 "" H 4400 4750 50  0001 C CNN
F 3 "" H 4400 4750 50  0001 C CNN
	1    4400 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5F1288A8
P 6000 4800
F 0 "#PWR0102" H 6000 4550 50  0001 C CNN
F 1 "GND" H 6005 4627 50  0000 C CNN
F 2 "" H 6000 4800 50  0001 C CNN
F 3 "" H 6000 4800 50  0001 C CNN
	1    6000 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 4250 4400 4750
Connection ~ 4400 4250
Wire Wire Line
	4300 3150 4300 2650
Connection ~ 4300 3150
$Comp
L power:+5V #PWR0104
U 1 1 5F152747
P 4300 2650
F 0 "#PWR0104" H 4300 2500 50  0001 C CNN
F 1 "+5V" H 4315 2823 50  0000 C CNN
F 2 "" H 4300 2650 50  0001 C CNN
F 3 "" H 4300 2650 50  0001 C CNN
	1    4300 2650
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0105
U 1 1 5F15538F
P 8900 2950
F 0 "#PWR0105" H 8900 2800 50  0001 C CNN
F 1 "+3V3" H 8915 3123 50  0000 C CNN
F 2 "" H 8900 2950 50  0001 C CNN
F 3 "" H 8900 2950 50  0001 C CNN
	1    8900 2950
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0106
U 1 1 5F155E54
P 6850 2950
F 0 "#PWR0106" H 6850 2800 50  0001 C CNN
F 1 "+3V3" V 6865 3078 50  0000 L CNN
F 2 "" H 6850 2950 50  0001 C CNN
F 3 "" H 6850 2950 50  0001 C CNN
	1    6850 2950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5F16A80A
P 6700 3500
F 0 "#PWR0107" H 6700 3250 50  0001 C CNN
F 1 "GND" H 6705 3327 50  0000 C CNN
F 2 "" H 6700 3500 50  0001 C CNN
F 3 "" H 6700 3500 50  0001 C CNN
	1    6700 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5F16B7B4
P 8500 4400
F 0 "#PWR0108" H 8500 4150 50  0001 C CNN
F 1 "GND" H 8505 4227 50  0000 C CNN
F 2 "" H 8500 4400 50  0001 C CNN
F 3 "" H 8500 4400 50  0001 C CNN
	1    8500 4400
	1    0    0    -1  
$EndComp
Connection ~ 6100 3150
$Comp
L power:+3V3 #PWR0109
U 1 1 5F151914
P 6100 2650
F 0 "#PWR0109" H 6100 2500 50  0001 C CNN
F 1 "+3V3" H 6115 2823 50  0000 C CNN
F 2 "" H 6100 2650 50  0001 C CNN
F 3 "" H 6100 2650 50  0001 C CNN
	1    6100 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3150 6100 2650
Connection ~ 4400 3650
Wire Wire Line
	4400 3650 4650 3650
Wire Wire Line
	4650 3050 4400 3050
Wire Wire Line
	4400 3050 4400 3650
Entry Wire Line
	2500 3050 2600 3150
Entry Wire Line
	2500 2900 2600 3000
Entry Wire Line
	2500 3650 2600 3750
Text Label 2350 3650 0    50   ~ 0
D52
Text Label 2350 3050 0    50   ~ 0
D53
Entry Wire Line
	3900 3350 4000 3250
Entry Wire Line
	3900 3050 4000 2950
Entry Wire Line
	3900 2950 4000 2850
Wire Wire Line
	4000 2850 4650 2850
Wire Wire Line
	4000 2950 4650 2950
Wire Wire Line
	4000 3250 4650 3250
Wire Wire Line
	4650 3350 4000 3350
Wire Wire Line
	4650 3450 4000 3450
Wire Wire Line
	4650 3550 4000 3550
Wire Wire Line
	4650 3850 4000 3850
Wire Wire Line
	4650 3950 4000 3950
Wire Wire Line
	4650 4050 4000 4050
Wire Wire Line
	4650 4150 4000 4150
Wire Wire Line
	4650 4450 4000 4450
Wire Wire Line
	4650 4550 4000 4550
Entry Wire Line
	3900 3450 4000 3350
Entry Wire Line
	3900 3550 4000 3450
Entry Wire Line
	3900 3650 4000 3550
Entry Wire Line
	3900 3950 4000 3850
Entry Wire Line
	3900 4050 4000 3950
Entry Wire Line
	3900 4150 4000 4050
Entry Wire Line
	3900 4250 4000 4150
Entry Wire Line
	3900 4550 4000 4450
Entry Wire Line
	3900 4650 4000 4550
Text Label 4500 2850 0    50   ~ 0
SDA
Text Label 4500 2950 0    50   ~ 0
SCL
Text Label 4500 3250 0    50   ~ 0
TX1
Text Label 4500 3350 0    50   ~ 0
RX1
Text Label 4500 3450 0    50   ~ 0
TX2
Text Label 4500 3550 0    50   ~ 0
RX2
Text Label 4500 3850 0    50   ~ 0
D8
Text Label 4500 3950 0    50   ~ 0
D9
Text Label 4500 4050 0    50   ~ 0
D7
Text Label 4500 4150 0    50   ~ 0
D53
Text Label 4500 4450 0    50   ~ 0
D52
Text Label 4500 4550 0    50   ~ 0
D51
Entry Wire Line
	6450 2850 6550 2950
Wire Wire Line
	5700 2850 6450 2850
Wire Wire Line
	5700 2950 6450 2950
Wire Wire Line
	5700 3250 6450 3250
Wire Wire Line
	5700 3350 6450 3350
Wire Wire Line
	5700 3450 6450 3450
Wire Wire Line
	5700 3550 6450 3550
Wire Wire Line
	5700 3850 6450 3850
Wire Wire Line
	5700 3950 6450 3950
Wire Wire Line
	5700 4050 6450 4050
Wire Wire Line
	5700 4150 6450 4150
Wire Wire Line
	5700 4450 6450 4450
Wire Wire Line
	5700 4550 6450 4550
Entry Wire Line
	6450 2950 6550 3050
Entry Wire Line
	6450 3250 6550 3350
Entry Wire Line
	6450 3350 6550 3450
Entry Wire Line
	6450 3450 6550 3550
Entry Wire Line
	6450 3550 6550 3650
Entry Wire Line
	6450 3850 6550 3950
Entry Wire Line
	6450 3950 6550 4050
Entry Wire Line
	6450 4050 6550 4150
Entry Wire Line
	6450 4150 6550 4250
Entry Wire Line
	6450 4450 6550 4550
Entry Wire Line
	6450 4550 6550 4650
Text Label 5750 2850 0    50   ~ 0
SDA_ll
Text Label 5750 2950 0    50   ~ 0
SCL_ll
Text Label 5750 3250 0    50   ~ 0
TX1_ll
Text Label 5750 3350 0    50   ~ 0
RX1_ll
Text Label 5750 3450 0    50   ~ 0
TX2_ll
Text Label 5750 3550 0    50   ~ 0
RX2_ll
Text Label 5750 3850 0    50   ~ 0
D8_ll
Text Label 5750 3950 0    50   ~ 0
D9_ll
Text Label 5750 4050 0    50   ~ 0
D7_ll
Text Label 5750 4150 0    50   ~ 0
D53_ll
Text Label 5750 4450 0    50   ~ 0
D52_ll
Text Label 5750 4550 0    50   ~ 0
D51_ll
Wire Notes Line
	4650 4700 4650 2600
Wire Notes Line
	4650 2600 5700 2600
Wire Notes Line
	5700 2600 5700 4700
Wire Notes Line
	5700 4700 4650 4700
Text Notes 4950 2600 0    50   ~ 0
Level shifters
Text Notes 1900 2650 0    50   ~ 0
RobotDyn MEGA2560 PRO
$Comp
L Connector_Generic:Conn_02x21_Odd_Even J1
U 1 1 5EF22623
P 3250 4000
F 0 "J1" H 3300 2775 50  0000 C CNN
F 1 "Conn_02x21" H 3300 2866 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x21_P2.54mm_Vertical" H 3250 4000 50  0001 C CNN
F 3 "~" H 3250 4000 50  0001 C CNN
	1    3250 4000
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5F16723F
P 3600 5100
F 0 "#PWR0110" H 3600 4850 50  0001 C CNN
F 1 "GND" H 3605 4927 50  0000 C CNN
F 2 "" H 3600 5100 50  0001 C CNN
F 3 "" H 3600 5100 50  0001 C CNN
	1    3600 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 4900 3600 4900
$Comp
L power:+5V #PWR0112
U 1 1 5F1789F1
P 3550 4800
F 0 "#PWR0112" H 3550 4650 50  0001 C CNN
F 1 "+5V" V 3565 4928 50  0000 L CNN
F 2 "" H 3550 4800 50  0001 C CNN
F 3 "" H 3550 4800 50  0001 C CNN
	1    3550 4800
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR0113
U 1 1 5F17F532
P 3050 4800
F 0 "#PWR0113" H 3050 4650 50  0001 C CNN
F 1 "+5V" V 3065 4928 50  0000 L CNN
F 2 "" H 3050 4800 50  0001 C CNN
F 3 "" H 3050 4800 50  0001 C CNN
	1    3050 4800
	0    -1   -1   0   
$EndComp
$Comp
L power:+3V3 #PWR0114
U 1 1 5F18D874
P 3550 4700
F 0 "#PWR0114" H 3550 4550 50  0001 C CNN
F 1 "+3V3" V 3565 4828 50  0000 L CNN
F 2 "" H 3550 4700 50  0001 C CNN
F 3 "" H 3550 4700 50  0001 C CNN
	1    3550 4700
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0115
U 1 1 5F18E1DC
P 3050 4700
F 0 "#PWR0115" H 3050 4550 50  0001 C CNN
F 1 "+3V3" V 3065 4828 50  0000 L CNN
F 2 "" H 3050 4700 50  0001 C CNN
F 3 "" H 3050 4700 50  0001 C CNN
	1    3050 4700
	0    -1   -1   0   
$EndComp
Entry Wire Line
	2600 3600 2700 3700
Entry Wire Line
	2600 3500 2700 3600
Entry Wire Line
	2600 3400 2700 3500
Wire Wire Line
	2700 3500 3050 3500
Wire Wire Line
	2700 3600 3050 3600
Wire Wire Line
	2700 3700 3050 3700
Wire Wire Line
	2700 4100 3050 4100
Entry Wire Line
	2600 4000 2700 4100
Text Label 2700 3500 0    50   ~ 0
SDA
Text Label 2700 3600 0    50   ~ 0
TX1
Text Label 2700 3700 0    50   ~ 0
TX2
Wire Wire Line
	3550 3500 3800 3500
Wire Wire Line
	3550 3600 3800 3600
Wire Wire Line
	3550 4100 3800 4100
Wire Wire Line
	3550 4200 3800 4200
Entry Wire Line
	3800 3500 3900 3400
Entry Wire Line
	3800 3600 3900 3500
Wire Wire Line
	3550 3700 3800 3700
Entry Wire Line
	3800 3700 3900 3600
Entry Wire Line
	3800 4100 3900 4000
Entry Wire Line
	3800 4200 3900 4100
Text Label 3650 3500 0    50   ~ 0
SCL
Text Label 3650 3600 0    50   ~ 0
RX1
Text Label 3650 3700 0    50   ~ 0
RX2
Text Label 3700 4100 0    50   ~ 0
D9
Text Label 3700 4200 0    50   ~ 0
D7
Text Label 2700 4100 0    50   ~ 0
D8
Text Label 3650 4600 0    50   ~ 0
RST
Wire Wire Line
	3550 4600 3800 4600
Wire Wire Line
	2300 3550 2300 3650
Wire Wire Line
	2300 3650 2500 3650
Wire Bus Line
	3750 2700 3900 2850
Wire Bus Line
	2600 2850 2750 2700
Wire Bus Line
	2750 2700 3750 2700
Wire Wire Line
	7050 3200 6650 3200
Wire Wire Line
	7050 3300 6650 3300
Entry Wire Line
	6550 3100 6650 3200
Entry Wire Line
	6550 3200 6650 3300
Text Label 6800 3200 0    50   ~ 0
SCL_ll
Text Label 6800 3300 0    50   ~ 0
SDA_ll
Wire Bus Line
	6550 2750 6700 2600
Wire Bus Line
	6700 2600 8200 2600
Wire Bus Line
	8200 2600 8350 2750
Wire Wire Line
	8000 3200 8250 3200
Wire Wire Line
	8000 3300 8250 3300
Entry Wire Line
	8250 3200 8350 3300
Entry Wire Line
	8250 3300 8350 3400
Text Label 8000 3200 0    50   ~ 0
TX1_ll
Text Label 8000 3300 0    50   ~ 0
RX1_ll
Wire Wire Line
	8900 3250 8450 3250
Wire Wire Line
	8900 3350 8450 3350
Wire Wire Line
	8900 3450 8450 3450
Wire Wire Line
	8900 3550 8450 3550
Wire Wire Line
	8900 3650 8450 3650
Entry Wire Line
	8350 3750 8450 3650
Entry Wire Line
	8350 3650 8450 3550
Entry Wire Line
	8350 3550 8450 3450
Entry Wire Line
	8350 3450 8450 3350
Entry Wire Line
	8350 3350 8450 3250
Entry Wire Line
	8350 3250 8450 3150
Text Label 8650 3250 0    50   ~ 0
D52_ll
Text Label 8650 3350 0    50   ~ 0
D53_ll
Text Label 8650 3450 0    50   ~ 0
D8_ll
Text Label 8650 3550 0    50   ~ 0
D9_ll
Text Label 8650 3650 0    50   ~ 0
D7_ll
Wire Notes Line
	7050 4650 8000 4650
Wire Notes Line
	8000 2550 7050 2550
Text Notes 7050 2550 0    50   ~ 0
Hardwario CO2 module
Text Notes 9000 2700 0    50   ~ 0
Screen
Text Notes 9350 4100 0    50   ~ 0
ESP-01
Text Notes 9200 2950 0    50   ~ 0
VCC
Text Notes 9200 3050 0    50   ~ 0
GND
Text Notes 9200 3150 0    50   ~ 0
DIN
Text Notes 9200 3250 0    50   ~ 0
CLK
Text Notes 9200 3350 0    50   ~ 0
CS
Text Notes 9200 3450 0    50   ~ 0
DC
Text Notes 9200 3550 0    50   ~ 0
RST
Text Notes 9200 3650 0    50   ~ 0
BUSY
Wire Wire Line
	10200 5250 10200 4700
Wire Wire Line
	9750 4700 10200 4700
Connection ~ 10200 4700
$Comp
L power:+3V3 #PWR0116
U 1 1 5F6F064B
P 10200 4150
F 0 "#PWR0116" H 10200 4000 50  0001 C CNN
F 1 "+3V3" H 10215 4323 50  0000 C CNN
F 2 "" H 10200 4150 50  0001 C CNN
F 3 "" H 10200 4150 50  0001 C CNN
	1    10200 4150
	1    0    0    -1  
$EndComp
Entry Wire Line
	8350 4600 8450 4700
Entry Wire Line
	8350 4000 8450 4100
Text Label 9750 4400 0    50   ~ 0
RX2_ll
Text Label 9750 4600 0    50   ~ 0
RST
Text Label 9000 4700 0    50   ~ 0
TX2_ll
Wire Wire Line
	8500 4400 9250 4400
Wire Wire Line
	8450 4700 9250 4700
Wire Wire Line
	8650 4500 8650 4850
Wire Wire Line
	8650 4500 9250 4500
Wire Wire Line
	8950 4850 8950 4600
Wire Wire Line
	8950 4600 9250 4600
Wire Wire Line
	8650 5250 8650 5150
Wire Wire Line
	8650 5250 8950 5250
Wire Wire Line
	8950 5150 8950 5250
Connection ~ 8950 5250
Wire Wire Line
	8950 5250 10050 5250
Wire Notes Line
	1300 2650 1300 5450
Wire Notes Line
	3500 5450 3500 2650
Wire Wire Line
	6100 3750 5700 3750
Connection ~ 6000 3650
Wire Wire Line
	6000 3650 6000 4250
Wire Wire Line
	5700 4250 6000 4250
Connection ~ 6000 4250
Wire Wire Line
	6000 4250 6000 4800
Wire Wire Line
	5700 3650 6000 3650
Wire Wire Line
	5700 3050 6000 3050
Wire Wire Line
	8900 3150 8450 3150
Wire Wire Line
	8550 3800 8550 3050
Text Label 8650 3150 0    50   ~ 0
D51_ll
Wire Wire Line
	8550 3050 8900 3050
Wire Wire Line
	10200 4150 10200 4700
Wire Wire Line
	8450 4100 10000 4100
Wire Wire Line
	9750 4400 10000 4400
Wire Wire Line
	9750 4600 10000 4600
Wire Wire Line
	10050 4500 10050 4850
Wire Wire Line
	9750 4500 10050 4500
Wire Wire Line
	10050 5150 10050 5250
Connection ~ 10050 5250
Wire Wire Line
	10050 5250 10200 5250
$Comp
L power:GND1 #PWR0103
U 1 1 5F8FDBAF
P 2900 5100
F 0 "#PWR0103" H 2900 4850 50  0001 C CNN
F 1 "GND1" H 2905 4927 50  0000 C CNN
F 2 "" H 2900 5100 50  0001 C CNN
F 3 "" H 2900 5100 50  0001 C CNN
	1    2900 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND1 #PWR0111
U 1 1 5F911B31
P 8550 3800
F 0 "#PWR0111" H 8550 3550 50  0001 C CNN
F 1 "GND1" H 8555 3627 50  0000 C CNN
F 2 "" H 8550 3800 50  0001 C CNN
F 3 "" H 8550 3800 50  0001 C CNN
	1    8550 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 4900 2900 4900
Wire Wire Line
	2300 3050 2500 3050
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J3
U 1 1 5EF2F859
P 2200 3350
F 0 "J3" V 2296 3162 50  0000 R CNN
F 1 "Conn_02x03" V 2205 3162 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x03_P2.54mm_Vertical" H 2200 3350 50  0001 C CNN
F 3 "~" H 2200 3350 50  0001 C CNN
	1    2200 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2200 2900 2500 2900
Text Label 2350 2900 0    50   ~ 0
D51
Wire Wire Line
	2200 3050 2200 2900
Wire Wire Line
	6700 3000 7050 3000
Wire Wire Line
	10000 4100 10000 4400
Wire Notes Line
	8000 2550 8000 4650
Wire Notes Line
	7050 2550 7050 4650
Wire Wire Line
	6700 3000 6700 3500
Wire Wire Line
	7050 3100 6850 3100
Wire Wire Line
	6850 3100 6850 2950
$Comp
L Connector:Conn_01x02_Female J10
U 1 1 5EEE1C4E
P 4850 5800
F 0 "J10" H 4878 5776 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4878 5685 50  0000 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 4850 5800 50  0001 C CNN
F 3 "~" H 4850 5800 50  0001 C CNN
	1    4850 5800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J11
U 1 1 5EEE2D8F
P 4850 6350
F 0 "J11" H 4878 6326 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4878 6235 50  0000 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 4850 6350 50  0001 C CNN
F 3 "~" H 4850 6350 50  0001 C CNN
	1    4850 6350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5EEEACBB
P 4650 6450
F 0 "#PWR0117" H 4650 6200 50  0001 C CNN
F 1 "GND" H 4655 6277 50  0000 C CNN
F 2 "" H 4650 6450 50  0001 C CNN
F 3 "" H 4650 6450 50  0001 C CNN
	1    4650 6450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5EEEEAB6
P 4650 5900
F 0 "#PWR0118" H 4650 5650 50  0001 C CNN
F 1 "GND" H 4655 5727 50  0000 C CNN
F 2 "" H 4650 5900 50  0001 C CNN
F 3 "" H 4650 5900 50  0001 C CNN
	1    4650 5900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0119
U 1 1 5EEF2ED4
P 4650 5800
F 0 "#PWR0119" H 4650 5650 50  0001 C CNN
F 1 "+5V" V 4665 5928 50  0000 L CNN
F 2 "" H 4650 5800 50  0001 C CNN
F 3 "" H 4650 5800 50  0001 C CNN
	1    4650 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4900 3600 5100
Text Label 3650 5000 0    50   ~ 0
VIN
Text Label 4450 6350 0    50   ~ 0
VIN
Text Notes 4600 6950 0    50   ~ 0
Optional power inputs
Wire Wire Line
	4000 5000 4000 6350
Wire Wire Line
	3550 5000 4000 5000
Wire Wire Line
	4000 6350 4650 6350
Wire Wire Line
	2900 4900 2900 5100
Wire Wire Line
	3050 5000 3050 5100
Wire Wire Line
	3050 5100 3550 5100
Wire Wire Line
	3550 5100 3550 5000
Connection ~ 3550 5000
Wire Notes Line
	3500 2650 1300 2650
Wire Notes Line
	1300 5450 3500 5450
Wire Notes Line
	4100 5350 4100 6800
Wire Notes Line
	4100 6800 5850 6800
Wire Notes Line
	5850 6800 5850 5350
Wire Notes Line
	5850 5350 4100 5350
Wire Bus Line
	2600 2850 2600 4500
Wire Bus Line
	8350 2750 8350 4750
Wire Bus Line
	6550 2750 6550 4800
Wire Bus Line
	3900 2850 3900 4700
$EndSCHEMATC
